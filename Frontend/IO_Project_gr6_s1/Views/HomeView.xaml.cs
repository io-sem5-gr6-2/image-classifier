﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace IO_Project_gr6_s1.Views
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class HomeView : System.Windows.Controls.UserControl
    {
        public HomeView()
        {
            InitializeComponent();
        }


        private void OpenInputFolder(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                InputFolderPath.Text = fbd.SelectedPath;
                CommandManager.InvalidateRequerySuggested();
                sendButton.IsEnabled = true;
            }
        }
        private void OpenInputFile(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Plix txt (.txt)|*.txt";

            // Display OpenFileDialog by calling ShowDialog method
            System.Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                InputFilePath.Text = dlg.FileName;
                CommandManager.InvalidateRequerySuggested();
                System.Diagnostics.Trace.WriteLine(InputFilePath.Text);

            }
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            sendButton.IsEnabled = false;
        }
    }
}

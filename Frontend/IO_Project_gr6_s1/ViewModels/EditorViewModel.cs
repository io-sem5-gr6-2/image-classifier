﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Drawing;
using IO_Project_gr6_s1.Data;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;
using System.Windows.Markup;
using System.Xml;

namespace IO_Project_gr6_s1.ViewModels
{
    public class EditorViewModel : BaseViewModel
    {
        private BitmapImage _image;
        /// <summary>
        /// source image of the photo
        /// </summary>
        public BitmapImage imageSource
        {
            get
            {
                return this._image;
            }
        }
        /*private Uri _uri;
        public Uri uri
        {
            set
            {
                this._uri = value;
            }
            get
            {
                return this._uri;
            }
        }*/


        /*private Canvas editorCanvas;
        public Canvas EditorCanvas
        {
            get { return editorCanvas; }
            set
            {
                editorCanvas = value;
            }
        }*/

        private string fileName;
        public EditorViewModel(ICommand _UpdateViewCommand, ImageObject io)
        {
            UpdateViewCommand = _UpdateViewCommand;
            using (FileStream fs = new FileStream(io.ImagePath, FileMode.Open))
            {
                _image = new BitmapImage();
                _image.BeginInit();
                _image.CacheOption = BitmapCacheOption.OnLoad;
                _image.StreamSource = fs;
                _image.EndInit();
                _image.Freeze();
            }

            Uri uri = new Uri(io.ImagePath, UriKind.Absolute);
            /*System.Diagnostics.Trace.WriteLine(uri);
            System.Diagnostics.Trace.WriteLine(io.ImagePath);*/
            /*editorCanvas = new Canvas();
            editorCanvas.Width = 1245;
            editorCanvas.Height = 665;
            editorCanvas.Background = System.Windows.Media.Brushes.Transparent;*/
            fileName = uri.ToString().Split('/')[uri.ToString().Split('/').Length - 1] + ".xaml";
            Directory.CreateDirectory("EditorSaves");
            System.IO.File.WriteAllText("EditorSaves/connection.txt", fileName);

        }   
        public ICommand UpdateViewCommand { get; set; }

    }
}

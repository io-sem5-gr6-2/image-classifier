package GroupLabels

import (
	"fmt"
	"testing"
)

func TestGroupByGreyscale(t *testing.T) {
	l := CreateLabel(&MeanGreyscale{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/BlackImage.png",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[0]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",0,keys[0]))
	}
}

func TestGroupByOrientation(t *testing.T) {
	l := CreateLabel(&DataOrientation{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/TestImage.jpg",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[hash("1")]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",hash("1"),keys[0]))
	}
}

func TestGroupByMake(t *testing.T) {
	l := CreateLabel(&CameraMake{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/TestImage.jpg",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[hash("Canon")]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",hash("Canon"),keys[0]))
	}
}

func TestGroupByModel(t *testing.T) {
	l := CreateLabel(&DataModel{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/TestImage.jpg",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[hash("Canon EOS 40D")]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",hash("Canon EOS 40D"),keys[0]))
	}
}

func TestGroupByResolution(t *testing.T) {
	l := CreateLabel(&DataResolution{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/TestImage.jpg",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[168]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",179,keys[0]))
	}
}

func TestGroupByDay(t *testing.T) {
	l := CreateLabel(&DataDate{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/TestImage.jpg",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[735113]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",735113,keys[0]))
	}
}

func TestGroupByHour(t *testing.T) {
	l := CreateLabel(&DataDateHour{})
	imgArr := []IndexedImage{{
		Index:  0,
		ImPath: "./TestImages/TestImage.jpg",
	}}
	g, _ := l.Group(imgArr,0)

	if len(g)!=1 {
		t.Fatal("Group amount wasn't 1")
	} else if len(g[0])!=1 {
		t.Fatal("Group 0 size wasn't 1")
	}
	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	_, exists := l.Data[17642727]
	if !exists {
		t.Fatal(fmt.Sprintf("Image statistic wasn't calculated properly. Expected: %d Got: %d",17642727,keys[0]))
	}
}


﻿using System.Windows.Input;

using IO_Project_gr6_s1.UpdateView;
using IO_Project_gr6_s1.Data;

namespace IO_Project_gr6_s1.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        /// <summary>
        /// data object that stores all photos and informations about it (group ID, addnotation, path etc)
        /// </summary>
        private DataMenager data;
        private BaseViewModel _selectedViewModel;
        /// <summary>
        /// currently shown window
        /// </summary>
        public BaseViewModel SelectedViewModel
        {
            get { return _selectedViewModel; }
            set
            {
                _selectedViewModel = value;
                OnPropertyChanged(nameof(SelectedViewModel));
            }
        }

        public ICommand UpdateViewCommand { get; set; }
        /// <summary>
        /// return DataMenager object
        /// </summary>
        /// <returns>DataMenager object</returns>
        public DataMenager GetData() { return this.data;  }
        public MainViewModel()
        {
            this.data = new DataMenager();
            UpdateViewCommand = new UpdateViewCommand(this);
            SelectedViewModel = new HomeViewModel(this.UpdateViewCommand);
        }


    }
}

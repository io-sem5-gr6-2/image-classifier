﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Navigation;
using IO_Project_gr6_s1.ViewModels.Editor;
using System.Windows;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Markup;
using System.Xml;
using System.Reflection;
using System;

namespace IO_Project_gr6_s1.Views
{
    /// <summary>
    /// Interaction logic for EditorView.xaml
    /// </summary>
    public partial class EditorView : UserControl
    {
        int x = 0;
        Label currentLabel = null;
        Ellipse currentEllipse = null;
        Rectangle currentRect = null;
        string draggedItem = null;
        public static T CloneXaml<T>(T source)
        {
            string xaml = XamlWriter.Save(source);
            StringReader sr = new StringReader(xaml);
            XmlReader xr = XmlReader.Create(sr);
            return (T)XamlReader.Load(xr);
            
        }
        public EditorView()
        {
            //var binding = BindingOperations.GetBinding(ImageUri, ImageBrush.ImageSourceProperty);
            
            InitializeComponent();
            string filename = "EditorSaves/" + File.ReadAllText("EditorSaves/connection.txt");

            System.Diagnostics.Trace.WriteLine("znalazlem plik szukam");
            if (System.IO.File.Exists(filename))
            {
                System.Diagnostics.Trace.WriteLine("znalazlem plik tekstowy");
                Canvas canvas;
                using (FileStream fs = new FileStream(filename, FileMode.Open))
                {
                    canvas = XamlReader.Load(fs) as Canvas;
                    foreach (UIElement child in canvas.Children)
                    {
                        var copy = CloneXaml(child);
                        EditorCanvas.Children.Add(copy);

                    }

                }
            }


        }
        

        private void EditorCanvas_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch(e.Source)
            {
                
                case Ellipse:
                    if (e.Source is Ellipse)
                    {
                        Ellipse activeObj = (Ellipse)e.Source;
                        EditorCanvas.Children.Remove(activeObj);
                    }               
                    break;
                case Label:
                    if (e.Source is Label)
                    {
                        Label activeObj = (Label)e.Source;
                        EditorCanvas.Children.Remove(activeObj);
                    }
                    break;
                case Rectangle:
                    if (e.Source is Rectangle)
                    {
                        Rectangle activeObj = (Rectangle)e.Source;
                        EditorCanvas.Children.Remove(activeObj);
                    }
                    break;
                default:
                    if (x==2)
                    {
                        Ellipse newObj = new Ellipse
                        {
                            Width = 50,
                            Height = 50,
                            Fill = Brushes.Transparent,
                            StrokeThickness = 3,
                            Stroke = Brushes.Red,
                            AllowDrop = true
                        };
                        Canvas.SetLeft(newObj, Mouse.GetPosition(EditorCanvas).X);
                        Canvas.SetTop(newObj, Mouse.GetPosition(EditorCanvas).Y);
                        EditorCanvas.Children.Add(newObj);
                    }
                    if (x==3)
                    {
                        LabelAnnotation newObj = new LabelAnnotation(100, 50, "rclick on me...", 16, Brushes.Red, FontWeights.Normal);

                        Canvas.SetLeft(newObj.GetLabel(), Mouse.GetPosition(EditorCanvas).X);
                        Canvas.SetTop(newObj.GetLabel(), Mouse.GetPosition(EditorCanvas).Y);
                        EditorCanvas.Children.Add(newObj.GetLabel());
                    }
                    if (x == 4)
                    {
                        Rectangle newObj = new Rectangle
                        {
                            Width = 50,
                            Height = 50,
                            Fill = Brushes.Transparent,
                            StrokeThickness = 3,
                            Stroke = Brushes.Red,
                            AllowDrop = true
                        };
                        Canvas.SetLeft(newObj, Mouse.GetPosition(EditorCanvas).X);
                        Canvas.SetTop(newObj, Mouse.GetPosition(EditorCanvas).Y);
                        EditorCanvas.Children.Add(newObj);
                    }
                    break;

            }
            DisplayLabelOptions.Visibility = Visibility.Hidden;
            DisplayEllipseOptions.Visibility = Visibility.Hidden;
            DisplayRectOptions.Visibility = Visibility.Hidden;
        }

        private void rect_Click(object sender, RoutedEventArgs e)
        {
            x = 4;
            DisplayLabelOptions.Visibility = Visibility.Hidden;
            DisplayEllipseOptions.Visibility = Visibility.Hidden;
            DisplayRectOptions.Visibility = Visibility.Hidden;
        }
        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            x = 2;
            DisplayLabelOptions.Visibility = Visibility.Hidden;
            DisplayEllipseOptions.Visibility = Visibility.Hidden;
            DisplayRectOptions.Visibility = Visibility.Hidden;
        }

        private void Button_Click_2(object sender, System.Windows.RoutedEventArgs e)
        {
            x = 3;
            DisplayLabelOptions.Visibility = Visibility.Hidden;
            DisplayEllipseOptions.Visibility = Visibility.Hidden;
            DisplayRectOptions.Visibility = Visibility.Hidden;
        }


        private void EditorCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.Source)
            {
                case Label:
                    if (e.Source is Label)
                    {
                        currentEllipse = null;
                        Label activeObj = (Label)e.Source;  
                        currentLabel = activeObj;
                        switch (activeObj.Foreground.ToString()){
                            case "#FFFF0000":
                                foregroundLbl.SelectedIndex = 0;
                                break;
                            case "#FF008000":
                                foregroundLbl.SelectedIndex = 1;
                                break;
                            case "#FF0000FF":
                                foregroundLbl.SelectedIndex = 2;
                                break;
                        }
                        switch (activeObj.FontWeight.ToString())
                        {
                            case "Normal":
                                fontweightlbl.SelectedIndex = 0;
                                break;
                            case "Bold":
                                fontweightlbl.SelectedIndex = 1;
                                break;
                            case "Light":
                                fontweightlbl.SelectedIndex = 2;
                                break;
                        }
                        draggedItem = "label";
                        ViewLabel(currentLabel);
                        DisplayLabelOptions.Visibility = Visibility.Visible;
                        DisplayEllipseOptions.Visibility = Visibility.Hidden;
                        DisplayRectOptions.Visibility = Visibility.Hidden;
                    }
                    break;
                case Ellipse:
                    if (e.Source is Ellipse)
                    {
                        
                        Ellipse activeObj = (Ellipse)e.Source;
                        currentEllipse = activeObj;
                        switch (activeObj.Stroke.ToString())
                        {
                            case "#FFFF0000":
                                colorEl.SelectedIndex = 0;
                                break;
                            case "#FF008000":
                                colorEl.SelectedIndex = 1;
                                break;
                            case "#FF0000FF":
                                colorEl.SelectedIndex = 2;
                                break;
                        }
                        draggedItem = "circle";
                        viewElllipse(currentEllipse);
                        DisplayEllipseOptions.Visibility = Visibility.Visible;
                        DisplayLabelOptions.Visibility = Visibility.Hidden;
                        DisplayRectOptions.Visibility = Visibility.Hidden;

                    }
                    break;
                case Rectangle:
                    if (e.Source is Rectangle)
                    {

                        Rectangle activeObj = (Rectangle)e.Source;
                        currentRect = activeObj;
                        switch (activeObj.Stroke.ToString())
                        {
                            case "#FFFF0000":
                                ColorRect.SelectedIndex = 0;
                                break;
                            case "#FF008000":
                                ColorRect.SelectedIndex = 1;
                                break;
                            case "#FF0000FF":
                                ColorRect.SelectedIndex = 2;
                                break;
                        }
                        draggedItem = "rect";
                        viewRect(currentRect);
                        DisplayEllipseOptions.Visibility = Visibility.Hidden;
                        DisplayLabelOptions.Visibility = Visibility.Hidden;
                        DisplayRectOptions.Visibility = Visibility.Visible;

                    }
                    break;
            }
            if (e.RightButton == MouseButtonState.Pressed)
            {
                if (draggedItem == "circle")
                {
                    DragDrop.DoDragDrop(currentEllipse, currentEllipse, DragDropEffects.Move);
                }
                if (draggedItem == "label")
                {
                    DragDrop.DoDragDrop(currentLabel, currentLabel, DragDropEffects.Move);
                }
                if (draggedItem == "rect")
                {
                    DragDrop.DoDragDrop(currentRect, currentRect, DragDropEffects.Move);
                }
            }
        }

        private void viewElllipse(Ellipse activeObj)
        {
            
            sizeEl.Text = activeObj.Width.ToString();
        }
        private void viewRect(Rectangle activeObj)
        {

            wRect.Text = activeObj.Width.ToString();
            hRect.Text = activeObj.Height.ToString();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void ViewLabel(Label activeObj)
        {
            widthLbl.Text = activeObj.Width.ToString();
            heightLbl.Text = activeObj.Height.ToString();
            contentLbl.Text = activeObj.Content.ToString();
            fontsizelbl.Text = activeObj.FontSize.ToString();
        }

        private void contentLbl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (currentLabel != null)
            {
                currentLabel.Content = contentLbl.Text;
            }
        }

        private void widthLbl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(currentLabel != null)
            {
               
                try
                {
                    currentLabel.Width = int.Parse(widthLbl.Text);
                }
                catch (System.FormatException err)
                {

                }
            }
            
        }

        private void heightLbl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (currentLabel != null)
            {
                try
                {
                    currentLabel.Height = int.Parse(heightLbl.Text);
                }
                catch (System.FormatException err)
                {

                }
                
            }
        }
        private void fontsizelbl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (currentLabel != null)
            {
                try
                {
                    currentLabel.FontSize = int.Parse(fontsizelbl.Text);
                }
                catch (System.FormatException err)
                {

                }

            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            string xaml = XamlWriter.Save(EditorCanvas);
            string filename = "EditorSaves/" + File.ReadAllText("EditorSaves/connection.txt");
            Directory.CreateDirectory("EditorSaves");
            System.IO.File.WriteAllText(filename, xaml);
        }

        private void ForegroundLbl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (currentLabel != null)
            {
                ComboBoxItem typeItem = (ComboBoxItem)foregroundLbl.SelectedItem;
                string value = typeItem.Content.ToString();
                Color selectedColor = (Color)ColorConverter.ConvertFromString(value);
                currentLabel.Foreground = new SolidColorBrush(selectedColor);
            }

            
        }

        private void Fontweightlbl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (currentLabel != null)
            {
                ComboBoxItem typeItem = (ComboBoxItem)fontweightlbl.SelectedItem;
                string value = typeItem.Content.ToString();
                FontWeight fw = (FontWeight)new FontWeightConverter().ConvertFromString(value);
                currentLabel.FontWeight = fw;
            }
        }

        

        private void sizeEl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (currentEllipse != null)
            {
                try
                {
                    currentEllipse.Width = int.Parse(sizeEl.Text);
                    currentEllipse.Height = int.Parse(sizeEl.Text);
                }
                catch (System.FormatException err)
                {

                }

            }
        }

        private void ColorEl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (currentEllipse != null)
            {
                ComboBoxItem typeItem = (ComboBoxItem)colorEl.SelectedItem;
                string value = typeItem.Content.ToString();
                Color selectedColor = (Color)ColorConverter.ConvertFromString(value);
                currentEllipse.Stroke = new SolidColorBrush(selectedColor);
            }
            
        }

        private void EditorCanvas_Drop(object sender, DragEventArgs e)
        {
            
        }

        private void EditorCanvas_DragOver(object sender, DragEventArgs e)
        {
            Point p = e.GetPosition(EditorCanvas);
            if(draggedItem == "circle")
            {
                Canvas.SetLeft(currentEllipse, p.X);
                Canvas.SetTop(currentEllipse, p.Y);
            }
            if(draggedItem == "label")
            {
                Canvas.SetLeft(currentLabel, p.X);
                Canvas.SetTop(currentLabel, p.Y);
            }
            if (draggedItem == "rect")
            {
                Canvas.SetLeft(currentRect, p.X);
                Canvas.SetTop(currentRect, p.Y);
            }

        }

        private void hRect_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (currentRect != null)
            {
                try
                {
                   
                    currentRect.Height = int.Parse(hRect.Text);
                }
                catch (System.FormatException err)
                {

                }

            }
        }

        private void wRect_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (currentRect != null)
            {
                try
                {
                    currentRect.Width = int.Parse(wRect.Text);
                   
                }
                catch (System.FormatException err)
                {

                }

            }
        }

        private void ColorRect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (currentRect != null)
            {
                ComboBoxItem typeItem = (ComboBoxItem)ColorRect.SelectedItem;
                string value = typeItem.Content.ToString();
                Color selectedColor = (Color)ColorConverter.ConvertFromString(value);
                currentRect.Stroke = new SolidColorBrush(selectedColor);
            }
        }
    }
}

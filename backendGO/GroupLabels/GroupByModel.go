package GroupLabels

import (
	"os"

	"github.com/rwcarlsen/goexif/exif"
	"github.com/tidwall/gjson"
)

type DataModel struct{}

//Function calculates label statistic value for provided imPath, takes Index provided from Frontend for ordering reasons
//After execution Data contains imIndex at key=calculated statistic, or if it existed previously, imIndex is appended to Duplicates at value = calculated statistic
func (imp *DataModel) computeFileStatistic(l *Label, imIndex uint32, imPath string) {
	defer l.waitGroup.Done()

	file, err := os.Open(imPath)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return

	}
	defer file.Close()

	//changed from img to exif
	metadata, err := exif.Decode(file)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}
	jsonByte, err := metadata.MarshalJSON()
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}

	jsonString := string(jsonByte)

	var model = gjson.Get(jsonString, "Model").String()

	/////////////////////////////////////////////////////////////////

	statistic := hash(model)

	//This is a not concurrent safe section, mutex must be locked
	l.mu.Lock()

	_, exists := l.Data[statistic]
	if exists {
		l.Duplicates[statistic] = append(l.Duplicates[statistic], imPath)
	} else {
		l.Data[statistic] = imPath
	}

	l.mu.Unlock()
}

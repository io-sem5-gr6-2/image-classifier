package GOCsAdapter

import (
	"backendGO/GroupLabels"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"

	socketio "github.com/googollee/go-socket.io"
)

type SingleFile struct {
	Path    string `json:"ImagePath"`
	GroupID string `json:"GroupID"`
}

type JsonRequest struct {
	Files    []SingleFile `json:"Files"`
	Distance float32      `json:"Distance"`
	Metric   int32        `json:"Metric"`
}

type IGOCsAdapter interface {
	initializeSocket()
}

type GOCsAdapter struct {
	server *socketio.Server
}

func (r *GOCsAdapter) InitializeSocket() {
	r.server = socketio.NewServer(nil)

	r.server.OnConnect("/", func(s socketio.Conn) error {
		fmt.Println("connected:", s.ID())
		return nil
	})

	r.server.OnEvent("/", "sendJson", func(s socketio.Conn, reqJson string) {
		var req JsonRequest
		err := json.Unmarshal([]byte(reqJson), &req)
		if err != nil {
			fmt.Println(err.Error())
		}
		images := r.openFiles(req.Files)
		label := GroupLabels.CreateLabel(&GroupLabels.DataResolution{})

		var distance uint32

		switch req.Metric {
		case 0:
			label = GroupLabels.CreateLabel(&GroupLabels.CameraMake{})
			distance = uint32(0)
		case 1:
			label = GroupLabels.CreateLabel(&GroupLabels.DataModel{})
			distance = uint32(0)
		case 2:
			label = GroupLabels.CreateLabel(&GroupLabels.DataOrientation{})
			distance = uint32(0)
		case 3:
			label = GroupLabels.CreateLabel(&GroupLabels.DataResolution{})
			distance = uint32(req.Distance * 5000)
		case 4:
			label = GroupLabels.CreateLabel(&GroupLabels.DataDate{})
			distance = uint32(math.Pow(3, float64(req.Distance*(6.9793))) - 1)
		case 5:
			label = GroupLabels.CreateLabel(&GroupLabels.DataDateHour{})
			distance = uint32(req.Distance * 1000)
		case 6:
			label = GroupLabels.CreateLabel(&GroupLabels.MeanGreyscale{})
			distance = uint32(req.Distance * 256)
		}

		groups, _ := label.Group(images, int32(distance))

		formattedJson, err := json.Marshal(groups)
		if err != nil {
			fmt.Println(err)
			return
		}

		var stringFormattedJson = string(formattedJson)

		s.Emit("getJson", stringFormattedJson)
		sum := 0
		for _, g := range groups {
			sum += len(g)
		}
	})

	go func() {
		if err := r.server.Serve(); err != nil {
			log.Fatalf("socketio listen error: %s\n", err)
		}
	}()
	defer r.server.Close()

	http.Handle("/socket.io/", r.server)

	log.Fatal(http.ListenAndServe(":10000", nil))
}

func (r *GOCsAdapter) openFiles(req []SingleFile) []GroupLabels.IndexedImage {
	var images []GroupLabels.IndexedImage

	for i, s := range req {
		images = append(images, GroupLabels.IndexedImage{Index: uint32(i), ImPath: s.Path})
	}

	return images
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace IO_Project_gr6_s1
{
    /// <summary>
    /// Converts command param to object array
    /// </summary>
    public class ParamConverter : IMultiValueConverter
    {
        /// <summary>
        /// converts command parameter to object array
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.Clone();
        }
        /// <summary>
        /// converts object array to obcject
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using IO_Project_gr6_s1.Data;

namespace IO_Project_gr6_s1.ViewModels
{
    public class SinglePhotoViewModel :BaseViewModel
    {
        private BitmapImage _image;
        private string _name; 
         /// <summary>
         /// name of photo
         /// </summary>
        public string imageName
        {
            set 
            { 
                this._name = value;
                OnPropertyChanged(nameof(imageName)); 
            }
            get { return _name; }
        }
        /// <summary>
        /// source image of the photo
        /// </summary>
        public BitmapImage imageSource
        {
            get { return _image; }
        }
        public SinglePhotoViewModel(ICommand _UpdateViewCommand, ImageObject IO)
        {
            using (FileStream fs = new FileStream(IO.ImagePath, FileMode.Open)) {
                _image = new BitmapImage();
                _image.BeginInit();
                _image.CacheOption = BitmapCacheOption.OnLoad;
                _image.StreamSource = fs;
                _image.EndInit();
                _image.DecodePixelHeight = 150;
                _image.DecodePixelWidth = 250;
                
                _image.Freeze();
            }

            this.UpdateViewCommand = _UpdateViewCommand;
            this.imageName = System.IO.Path.GetFileName(IO.ImagePath);
        }
        public ICommand UpdateViewCommand { get; set; }
    }
}

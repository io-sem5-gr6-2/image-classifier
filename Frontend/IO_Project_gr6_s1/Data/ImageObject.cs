﻿namespace IO_Project_gr6_s1.Data
{
    /// <summary>
    /// Object representing Image 
    /// </summary>
    public class ImageObject
    {
        /// <summary>
        /// Total path to Image
        /// </summary>
        public string ImagePath { get; }
        /// <summary>
        /// ID of group depending on choosen metricks
        /// </summary>
        public string GroupID { get; set; }
        /// <summary>
        /// Bool information if AddnotationPath Exist
        /// </summary>
        public bool AdnotationExist;
        /// <summary>
        /// Total Path for Image Adnotation file
        /// </summary>
        public string AdnotationPath;



        /// <summary>
        /// Constructor of ImageObject
        /// </summary>
        /// <param name="_ImagePath">Total path to image</param>
        /// <param name="_GroupID">Id of Group igage belongs to</param>
        /// <param name="_AddnotationExist">boolean information if addnotation exist</param>
        /// <param name="_AddnotationPath">can be empty if adnotation doesn't exist</param>
        public ImageObject(string _ImagePath, string _GroupID, bool _AddnotationExist, string _AddnotationPath)
        {
            this.ImagePath = _ImagePath;
            this.GroupID = _GroupID;
            this.AdnotationExist = _AddnotationExist;
            this.AdnotationPath = _AddnotationPath;
        }
    }
}

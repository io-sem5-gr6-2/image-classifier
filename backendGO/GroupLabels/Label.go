package GroupLabels

import (
	"hash/fnv"
	"sort"
	"sync"
)

type Label struct {

	//key - value of statistic value - image Index (first found with statistic value = key)
	Data       map[int32]string

	//key - value of statistic value - array of image indexes
	Duplicates map[int32] []string

	mu sync.Mutex

	waitGroup sync.WaitGroup

	calculatorImplementation LabelCalculator
}

func CreateLabel(calculator LabelCalculator) Label {
	var l Label
	l.Data = make(map[int32]string)
	l.Duplicates = make(map[int32][]string)

	l.calculatorImplementation=calculator

	return l
}

func (l *Label) Group(images []IndexedImage, distance int32) ([][]string, [][]string) {

	for _, s := range images {
		l.waitGroup.Add(1)
		l.calculatorImplementation.computeFileStatistic(l, s.Index, s.ImPath)
	}

	l.waitGroup.Wait()

	keys := make([]int32, 0)
	for k := range l.Data {
		keys = append(keys, k)
	}
	sort.Slice(keys,func(i, j int) bool { return keys[i] < keys[j] })

	var groups [][]string
	var duplicates [][]string

	groups = append(groups,[]string{l.Data[keys[0]]})
	groupIndex := 0
	duplicates0, exist0 := l.Duplicates[keys[0]]
	if exist0{
		groups[groupIndex] = append(groups[groupIndex], duplicates0...)
		duplicates0 = append(duplicates0, l.Data[keys[0]])
		duplicates = append(duplicates, duplicates0)
	}

	for i, k := range keys[1:] {
		if keys[i]+distance>k {
			groups[groupIndex] = append(groups[groupIndex],l.Data[k])
		} else {
			groups = append(groups,[]string{l.Data[k]})
			groupIndex++
		}

		//Check if duplicates exist
		currentDuplicates, exist := l.Duplicates[k]
		if exist{
			//If so add them to the group, and add a new array of duplicate image indexes to duplicates array
			groups[groupIndex] = append(groups[groupIndex], currentDuplicates...)
			currentDuplicates = append(currentDuplicates, l.Data[k])
			duplicates = append(duplicates, currentDuplicates)
		}
	}

	return groups, duplicates
}

func hash(s string) int32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return int32(h.Sum32())
}

type IndexedImage struct {
	Index  uint32
	ImPath string
}

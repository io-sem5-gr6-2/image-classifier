﻿using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using IO_Project_gr6_s1.ViewModels;

namespace IO_Project_gr6_s1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }
        void onProccessClosed(object sender, CancelEventArgs e)
        {
            Process[] processes = Process.GetProcessesByName("backendGO");

            foreach (Process p in processes)
            {
                p.CloseMainWindow();
                p.Close();
            }
        }

    }
}
package main

import (
	"backendGO/GOCsAdapter"
	_ "image/jpeg"

	_ "github.com/rwcarlsen/goexif/exif"
	_ "github.com/rwcarlsen/goexif/tiff"
)

func main() {

	var server GOCsAdapter.GOCsAdapter
	server.InitializeSocket()
}

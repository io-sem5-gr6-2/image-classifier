﻿using System.Windows.Input;
using IO_Project_gr6_s1.Data;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System;

namespace IO_Project_gr6_s1.ViewModels
{
    public class DashboardViewModel : BaseViewModel
    {
        public DashboardViewModel(ICommand _UpdateViewCommand, System.Collections.Generic.List<ImageObject> _FileList, int groupcount, int activeMetric, double _distance)
        {
            this._sliderValue = "";
            this.UpdateViewCommand = _UpdateViewCommand;
            this._metrics = new bool[7] { false, false, false, false, false, false, false };
            SelectedMetric = 1;
            this.distance = _distance;
            Files = new System.Collections.ObjectModel.ObservableCollection<SingleGroup>();
            System.Collections.ObjectModel.ObservableCollection<SinglePhotoViewModel> tmp;

            for (int i = 1; i <= groupcount; i++)
            {
                tmp = new System.Collections.ObjectModel.ObservableCollection<SinglePhotoViewModel>();

                foreach (ImageObject IO in _FileList.Where(IO => IO.GroupID == i.ToString()))
                {
                    tmp.Add(new SinglePhotoViewModel(this.UpdateViewCommand, IO));
                }

                Files.Add(new SingleGroup(tmp));
            }


            tmp = new System.Collections.ObjectModel.ObservableCollection<SinglePhotoViewModel>();
            foreach (ImageObject IO in _FileList.Where(IO => IO.GroupID == "0"))
            {
                tmp.Add(new SinglePhotoViewModel(this.UpdateViewCommand, IO));
            }
            if (activeMetric != -1)
                _metrics[activeMetric] = true;

            Files.Add(new SingleGroup(tmp));
        }
        public ICommand UpdateViewCommand { get; set; }
        /// <summary>
        /// collection of SingleGroup items
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<SingleGroup> Files { get; private set; }


        private bool[] _metrics;
        /// <summary>
        /// array of bool values to determin which metric is choosen
        /// </summary>
        public bool[] metrics
        {
            get { SliderValue(); SelectedMetric = 1; return _metrics; }
        }

        private void SliderValue()
        {

            if (_metrics[0] == true)
            {
                sliderValue = "";
            }
            if (_metrics[1] == true)
            {
                sliderValue = "";
            }
            if (_metrics[2] == true)
            {
                sliderValue = "";
            }
            if (_metrics[3] == true)
            {
                sliderValue = Convert.ToInt32(_distance*5000) + " px";
            }
            if (_metrics[4] == true)
            {
                sliderValue = Convert.ToInt32(Math.Pow(3, _distance*(6.9793)) - 1) + " days";
            }
            if (_metrics[5] == true)
            {
                sliderValue = Convert.ToInt32(_distance * 1000) + " h";
            }
            if (_metrics[6] == true)
            {
                sliderValue = Convert.ToInt32(_distance * 100) + " %";
            }
        }

        private int _selectedMetric;
        /// <summary>
        /// returns index of choosen metric without handling 
        /// </summary>
        public int SelectedMetric
        {
            set
            {
                _selectedMetric = - 1;
                for (int i = 0; i < _metrics.Length; i++)
                {
                    if (_metrics[i] == true)
                        _selectedMetric = i;
                }
                OnPropertyChanged("SelectedMetric");
            }
            get
            {
                return _selectedMetric;
            }
        }

        private string _sliderValue;

        public string sliderValue
        {
            get
            {
                return _sliderValue;
            }
            private set
            {
                _sliderValue = value;
                OnPropertyChanged("sliderValue");
            }

        }

        private double _distance;
        /// <summary>
        /// value of distance based on input slider
        /// </summary>
        public double distance
        {
            get { SliderValue(); return _distance; }
            set
            {
                _distance = System.Math.Floor(value * 100) / 100;
            }
        }
    }
    /// <summary>
    /// class represention SingleGroup objects containing collection of SinglePhoto objects
    /// </summary>
    public class SingleGroup
    {
        public System.Collections.ObjectModel.ObservableCollection<SinglePhotoViewModel> Group { get; set; }

        public SingleGroup(System.Collections.ObjectModel.ObservableCollection<SinglePhotoViewModel> group)
        {
            Group = group;
        }
    }

}

﻿using System;
using System.IO;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using IO_Project_gr6_s1.Data;

namespace IO_Project_gr6_s1.ViewModels
{
    public class  PhotoViewModel : BaseViewModel
    {
        private BitmapImage _image;
        private string fileName;
        /// <summary>
        /// source image of the photo
        /// </summary>
        public BitmapImage imageSource
        {
            get
            {
                return this._image;
            }
        }
        public PhotoViewModel(ICommand _UpdateViewCommand, ImageObject io)
        {
            this.UpdateViewCommand = _UpdateViewCommand;
            using (FileStream fs = new FileStream(io.ImagePath, FileMode.Open))
            {
                _image = new BitmapImage();
                _image.BeginInit();
                _image.CacheOption = BitmapCacheOption.OnLoad;
                _image.StreamSource = fs;
                _image.EndInit();
                _image.Freeze();
            }
            Uri uri = new Uri(io.ImagePath, UriKind.Absolute);
            fileName = uri.ToString().Split('/')[uri.ToString().Split('/').Length - 1] + ".xaml";
            Directory.CreateDirectory("EditorSaves");
            System.IO.File.WriteAllText("EditorSaves/connection.txt", fileName);
        }
        public ICommand UpdateViewCommand { get; set; }
    }
}

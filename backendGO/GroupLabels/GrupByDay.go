package GroupLabels

import (
	"os"

	"github.com/rwcarlsen/goexif/exif"
)

type DataDate struct{}

//Function calculates label statistic value for provided imPath, takes Index provided from Frontend for ordering reasons
//After execution Data contains imIndex at key=calculated statistic, or if it existed previously, imIndex is appended to Duplicates at value = calculated statistic
func (imp *DataDate) computeFileStatistic(l *Label, imIndex uint32, imPath string) {
	defer l.waitGroup.Done()

	file, err := os.Open(imPath)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}
	defer file.Close()

	im, err := exif.Decode(file)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}

	/////////////////////////////////////////////////////////////////
	// grouping by summing year * 366 + mc * 31 + day
	/////////////////////////////////////////////////////////////////

	var photoDate, _ = im.DateTime()
	var da = photoDate.Day()
	var mc = photoDate.Month()
	var ye = photoDate.Year()

	var dataSum = int32(da) + int32(mc*31) + int32(ye*366)

	statistic := dataSum

	//////////////////////////////////////////////////

	//This is a not concurrent safe section, mutex must be locked
	l.mu.Lock()

	_, exists := l.Data[statistic]
	if exists {
		l.Duplicates[statistic] = append(l.Duplicates[statistic], imPath)
	} else {
		l.Data[statistic] = imPath
	}

	l.mu.Unlock()
}

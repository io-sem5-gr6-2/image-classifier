﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace IO_Project_gr6_s1.ViewModels.Editor
{
    class LabelAnnotation
    {
        
        private Label lbl;
        

        public LabelAnnotation(int w, int h, string c, int fs, SolidColorBrush b, FontWeight fw) {

            this.lbl = new Label
            {
                Width = w,
                Height = h,
                Background = Brushes.Transparent,
                Content = c,
                FontSize = fs,
                Foreground = b,
                FontWeight = fw,
                AllowDrop = true
            };
        }
        public void updateHeight(int i)
        {
            this.lbl.Height = i;
        }
        public void updateWidth(int i)
        {
            this.lbl.Width = i;
        }
        public void updateContent(string c)
        {
            this.lbl.Content = c;
        }
        public void updateFontSize(int i)
        {
            this.lbl.FontSize = i;
        }
        public void updateFontWeight(FontWeight fw)
        {
            this.lbl.FontWeight = fw;
        }
        public void updateColor(SolidColorBrush b)
        {
            this.lbl.Foreground = b;
        }
        public Label GetLabel()
        {
            return lbl;
        }
        

    }
}

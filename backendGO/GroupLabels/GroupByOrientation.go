package GroupLabels

import (
	"os"
	"strings"

	"github.com/rwcarlsen/goexif/exif"
)

type DataOrientation struct{}

//Function calculates label statistic value for provided imPath, takes Index provided from Frontend for ordering reasons
//After execution Data contains imIndex at key=calculated statistic, or if it existed previously, imIndex is appended to Duplicates at value = calculated statistic
func (imp *DataOrientation) computeFileStatistic(l *Label, imIndex uint32, imPath string) {
	defer l.waitGroup.Done()

	file, err := os.Open(imPath)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}
	defer file.Close()

	//changed from img to exif
	metadata, err := exif.Decode(file)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}
	jsonByte, err := metadata.MarshalJSON()
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}

	jsonString := string(jsonByte)

	/////////////////////////////////////////////////////////////////
	// grouping by orientation number
	/////////////////////////////////////////////////////////////////

	var n = strings.Index(jsonString, "Orientation")
	if n == -1 {
		return
	}

	var dataSum = string(jsonString[n+14]) //uint32((orientation) - '0')

	/////////////////////////////////////////////////////////////////

	statistic := hash(dataSum)

	l.mu.Lock()

	_, exists := l.Data[statistic]
	if exists {
		l.Duplicates[statistic] = append(l.Duplicates[statistic], imPath)
	} else {
		l.Data[statistic] = imPath
	}

	l.mu.Unlock()
}

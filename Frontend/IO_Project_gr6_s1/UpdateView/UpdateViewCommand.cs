﻿using IO_Project_gr6_s1.Data;
using IO_Project_gr6_s1.ViewModels;
using SocketIOClient;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Input;
using System.Text.Json;
using System.Windows.Forms;

namespace IO_Project_gr6_s1.UpdateView
{
    /// <summary>
    /// UpdateVewCommand class handle all kind of window switches, it checks if window can be
    /// swiched, and if it can than process to do funny things like parasing object to array of
    /// objects, which are parased to strings which sometimes can be parased to floats etc
    /// </summary>
    public class UpdateViewCommand : ICommand
    {
        private MainViewModel viewModel;
        private BaseViewModel photo;
        private ImageObject img;
        private bool onlyFront = false;
        private string _jsonData;
        private bool canBeSaved;

        private bool problemWithConnection = false;

        private SocketIO socket;

        /// <summary>
        /// constructor of UpdateViewCommand
        /// </summary>
        /// <param name="viewModel">MainViewModel</param>
        public UpdateViewCommand(MainViewModel viewModel)
        {
            this.canBeSaved = false;
            socket = new SocketIO("http://localhost:10000/", new SocketIOOptions
            {
                EIO = 3
            });
            socket.ConnectAsync();

            socket.OnConnected += (sender, e) =>
            {
                if (problemWithConnection)
                    showMessageBox(false, "Success", "Successfully connected to the backend application!");

                problemWithConnection = false;
            };
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// bool function that checks if command can be executed
        /// </summary>
        /// <param name="parameter">
        /// object containing destination view, source view, and sometimes some parameters
        /// </param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            var values = (object[])parameter;

            if (values != null)
            {
                if (values[0].ToString() == "Home")
                {
                    if (String.IsNullOrEmpty(values[2].ToString()))
                    {
                        return false;
                    }
                    else
                    {
                        if (values[2].ToString() == "path")
                        {
                            return false;
                        }
                    }
                }
                if (values[0].ToString() == "Dashboard")
                {
                    if (values[1].ToString() == "Dashboard")
                    {
                        if (values[2].ToString() == "-1")
                        {
                            return false;
                        }
                        return true;
                    }
                    return true;
                }
                if (values[0].ToString() == "Save" && values[1].ToString() == "Save" && canBeSaved)
                {
                    if (String.IsNullOrEmpty(values[2].ToString()) || String.IsNullOrEmpty(values[3].ToString()))
                    {
                        return false;
                    }
                    else
                    {
                        if (values[2].ToString() == "-1")
                            return false;
                    }

                }
            }
            return true;
        }

        /// <summary>
        /// funccion to execute ViewUpdate, sets new view to selectedviewmodel in viewModel
        /// </summary>
        /// <param name="parameter">
        /// object containing destination view, source view, and sometimes some parameters
        /// </param>
        public void Execute(object parameter)
        {
            var values = (object[])parameter;
            if (values[1].ToString() == "Home")
            {
                viewModel.SelectedViewModel = new HomeViewModel(this);
            }
            else if (values[1].ToString() == "Dashboard")
            {
                if (!socket.Connected && values[0].ToString() == "Home" && !onlyFront)
                {
                    problemWithConnection = true;
                    showMessageBox(true, "Error!", "Client is not connected to the backend! Please open the backend app or restart the whole application.");
                }

                Thread loadingImagesThread = new Thread(() =>
                {
                    if (values[0].ToString() == "Home")
                    {
                        if (socket.Connected || onlyFront)
                        {
                            viewModel.GetData().SetFolderPath(values[2].ToString());
                            viewModel.GetData().FileListInit();
                            int metric = -1;
                            double _distance =0;
                            if (String.IsNullOrEmpty(values[3].ToString()) == false)
                            {
                                viewModel.GetData().SetFilePath(values[3].ToString());
                                string text = System.IO.File.ReadAllText(viewModel.GetData().GetFilePath());
                                if (text[0] == '{' && text[2] == '}' && text[3] == '{')
                                {
                                    metric = int.Parse(text[1].ToString());
                                    float distance = 10;
                                    if (text[5] == '}')
                                    {
                                        distance = float.Parse(text.Substring(4, 1), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                                        text = text.Substring(6);
                                    }
                                    else if (text[7] == '}')
                                    {
                                        distance = float.Parse(text.Substring(4, 1) + '.' + text.Substring(6, 1), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                                        text = text.Substring(8);
                                    }
                                    else if (text[8] == '}')
                                    {
                                        distance = float.Parse(text.Substring(4, 1) + '.' + text.Substring(6, 2), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                                        text = text.Substring(9);
                                    }
                                    _distance = double.Parse(distance.ToString());
                                    viewModel.GetData().UpdateFileList(text, metric);
                                }
                            }

                            viewModel.SelectedViewModel = new DashboardViewModel(this, viewModel.GetData().GetFileList(), viewModel.GetData().GroupCount(), metric, _distance);
                        }
                    }
                    else if (values[0].ToString() == "Dashboard" && !onlyFront)
                    {
                        var jsonObject = new Dictionary<string, object>();
                        jsonObject["Files"] = viewModel.GetData().GetFileList();
                        jsonObject["Distance"] = float.Parse(values[3].ToString(), System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                        int ametric = int.Parse(values[2].ToString());
                        jsonObject["Metric"] = ametric;
                        this.viewModel.GetData().activeMetric = ametric;
                        socket.EmitAsync("sendJson", JsonSerializer.Serialize(jsonObject));


                        socket.On("getJson", response =>
                        {
                            string jsonData = response.GetValue<string>();
                            _jsonData = "{" + ametric.ToString() + "}" + "{" + float.Parse(values[3].ToString(), System.Globalization.CultureInfo.InvariantCulture.NumberFormat) + "}" + jsonData;
                            viewModel.GetData().UpdateFileList(jsonData, ametric);
                            viewModel.SelectedViewModel = new DashboardViewModel(this, viewModel.GetData().GetFileList(), viewModel.GetData().GroupCount(), ametric, double.Parse(float.Parse(values[3].ToString(), System.Globalization.CultureInfo.InvariantCulture.NumberFormat).ToString()));
                            this.canBeSaved = true;
                        });
                    }
                    else
                    {
                        viewModel.SelectedViewModel = new DashboardViewModel(this, viewModel.GetData().GetFileList(), viewModel.GetData().GroupCount(), viewModel.GetData().activeMetric, 0);
                        //if addnotation is added change viewModel.GetData() single item depending on path
                    }
                });

                loadingImagesThread.Start();
            }
            else if (values[1].ToString() == "Save")
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Text File|*.txt";
                saveFileDialog1.Title = "Save Metric File";
                saveFileDialog1.ShowDialog();

                if (!String.IsNullOrEmpty(saveFileDialog1.FileName))
                {
                    System.IO.File.WriteAllText(saveFileDialog1.FileName, _jsonData);
                    string text = System.IO.File.ReadAllText(saveFileDialog1.FileName);
                    System.Diagnostics.Trace.WriteLine(text);
                }
                else System.Diagnostics.Trace.WriteLine("no path found");
            }
            else if (values[1].ToString() == "Photo")
            {
                if (values[0].ToString() == "Dashboard")
                {
                    photo = new PhotoViewModel(this, viewModel.GetData().getImageFromName(values[2].ToString()));
                    viewModel.SelectedViewModel = photo;
                    img = viewModel.GetData().getImageFromName(values[2].ToString());
                }
                else
                {
                    viewModel.SelectedViewModel = photo;
                }
            }
            else if (values[1].ToString() == "Editor")
            {
                viewModel.SelectedViewModel = new EditorViewModel(this, img);
            }
        }

        public void showMessageBox(bool _error, string _caption, string _text)
        {
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = _error ? MessageBoxImage.Error : MessageBoxImage.Information;

            System.Windows.MessageBox.Show(_text, _caption, button, icon, MessageBoxResult.Yes);
        }
    }
}

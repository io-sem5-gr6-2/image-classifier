package GroupLabels

type LabelCalculator interface {
	computeFileStatistic(l *Label, imIndex uint32, imPath string)
}

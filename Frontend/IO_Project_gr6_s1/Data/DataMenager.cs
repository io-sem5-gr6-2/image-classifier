﻿using System.Collections.Generic;
using System.IO;

namespace IO_Project_gr6_s1.Data
{
    /// <summary>
    /// Class containing data used in program
    /// </summary>
    public class DataMenager
    {
        /// <summary>
        /// Total path to folder containing images
        /// </summary>
        private string FolderPath;
        /// <summary>
        /// Total path to file with groups
        /// </summary>
        private string FilePath;
        /// <summary>
        /// number of existing groups
        /// </summary>
        private int groupCount;
        /// <summary>
        /// funcion returning number of groups
        /// </summary>
        /// <returns>groupCount</returns>
        public int GroupCount() { return this.groupCount; }

        public int activeMetric;


        /// <summary>
        /// List of images in Folder 
        /// </summary>
        private List<ImageObject> FileList;
        /// <summary>
        /// returns single photo object based on name
        /// </summary>
        /// <param name="name">name of photo</param>
        /// <returns>ImageObject</returns>
        public ImageObject getImageFromName(string name)
        {
            foreach(ImageObject io in this.FileList)
            {
                if (io.ImagePath.EndsWith(name))
                    return io;
            }
            return new ImageObject("","",false,"");
        }
        /// <summary>
        /// returns path of photo folder
        /// </summary>
        /// <returns>string path</returns>
        public string GetFolderPath()  { return this.FolderPath; }
        /// <summary>
        /// sets path to file with created groups
        /// </summary>
        /// <param name="_FilePath">path to file</param>
        public void SetFilePath(string _FilePath) { this.FilePath = _FilePath; }
        /// <summary>
        /// returns path to file with groups
        /// </summary>
        /// <returns>path to file</returns>
        public string GetFilePath() { return this.FilePath; }
        /// <summary>
        /// sets path to photo folder
        /// </summary>
        /// <param name="_FolderPath">path</param>
        public void SetFolderPath(string _FolderPath) { this.FolderPath = _FolderPath; }
        /// <summary>
        /// returns list of ImageObject based on choosen group ID
        /// </summary>
        /// <param name="group">ID of group</param>
        /// <returns>list of ImageObjects</returns>
        public List<ImageObject> GetFileList(string group) {
            List<ImageObject> tmp = new List<ImageObject>();
            foreach (ImageObject image in FileList)
            {
                if (image.GroupID == group)
                    tmp.Add(image);
            }
            return tmp; 
        }
        /// <summary>
        /// returns list of all ImageObject 
        /// </summary>
        /// <returns>list of ImageObject</returns>
        public List<ImageObject> GetFileList() { return this.FileList; }
        /// <summary>
        /// Make new image list with ealier passed path
        /// is looking for files "*.jpg/png/bmp/jpeg"
        /// List init depends on file passed (file can be empty)
        /// </summary>
        public void FileListInit()
        {
            if (!string.IsNullOrWhiteSpace(this.FolderPath))
            {
                if (!string.IsNullOrWhiteSpace(this.FilePath))
                {
                    //to do
                }
                else
                {
                    string[] files = Directory.GetFiles(this.FolderPath);

                    foreach(string file in files)
                    {
                        if (IsImage(file))
                        {
                            ImageObject img = new ImageObject(file, "0", false, "");
                            FileList.Add(img);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// updates FileList based on JSON from backend, parases json to twodimencional array and for each ImageObject in existing FileList, looks for group ID
        /// </summary>
        /// <param name="jsonData">Data from backend</param>
        public void UpdateFileList(string jsonData, int _activeMetric)
        {
            string[][] json = Newtonsoft.Json.JsonConvert.DeserializeObject<string[][]>(jsonData);
            bool next = false;
            this.groupCount = 1;
            foreach (ImageObject image in FileList)
            {
                next = false;
                int i = 0;
                foreach (string[] group in json)
                {
                    i++;
                    foreach (string path in group)
                    {
                        if (path == image.ImagePath )
                        {
                            image.GroupID = i.ToString();
                            if (i >= this.groupCount)
                                this.groupCount++;
                            next = true;
                            break;
                        }
                    }
                    if (next)
                        break;
                }
            }
            this.activeMetric = _activeMetric;
            System.Diagnostics.Trace.WriteLine(groupCount);
        }
        /// <summary>
        /// checks if file in folder is Image
        /// </summary>
        /// <param name="_name">name of file with extension</param>
        /// <returns>true of false</returns>
        private bool IsImage(string _name)
        {
            string[] extensions = { ".jpg", ".png", ".bmp", ".jpeg" };
            foreach (string n in extensions)
            {
                if (_name.EndsWith(n))
                {

                    return true; }
            }
            return false;
        } 
        /// <summary>
        /// sets FileList
        /// </summary>
        /// <param name="_FileList">given List of ImageObjects</param>
        public void SetFileList(List<ImageObject> _FileList) { this.FileList = _FileList; }
        public DataMenager()
        {
            this.FolderPath = "";
            this.FilePath = "";
            this.groupCount = 1;
            this.activeMetric = -1;
            this.FileList = new List<ImageObject>();
        }
    }

}

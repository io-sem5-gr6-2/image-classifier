package GroupLabels

import (
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

type MeanGreyscale struct {
}

//Function calculates label statistic value for provided imPath, takes Index provided from Frontend for ordering reasons
//After execution Data contains imIndex at key=calculated statistic, or if it existed previously, imIndex is appended to Duplicates at value = calculated statistic
func (imp *MeanGreyscale) computeFileStatistic(l *Label, imIndex uint32, imPath string) {
	defer l.waitGroup.Done()

	file, err := os.Open(imPath)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}

	defer file.Close()

	im, _, err := image.Decode(file)
	if err != nil {
		return
	}

	bounds := im.Bounds()

	var colors [3]int64
	var imSize = (bounds.Max.Y - bounds.Min.Y) * (bounds.Max.X - bounds.Min.X)

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			r, g, b, a := im.At(x, y).RGBA()

			if a != 0 {
				colors[0] += int64(r * 255 / a)
				colors[1] += int64(g * 255 / a)
				colors[2] += int64(b * 255 / a)
			} else {
				imSize--
			}
		}
	}

	var meanR = int32(colors[0] / int64(imSize))
	var meanG = int32(colors[1] / int64(imSize))
	var meanB = int32(colors[2] / int64(imSize))

	statistic := (meanB + meanG + meanR) / 3

	//This is a not concurrent safe section, mutex must be locked
	l.mu.Lock()

	_, exists := l.Data[statistic]
	if exists {
		l.Duplicates[statistic] = append(l.Duplicates[statistic], imPath)
	} else {
		l.Data[statistic] = imPath
	}

	l.mu.Unlock()
}

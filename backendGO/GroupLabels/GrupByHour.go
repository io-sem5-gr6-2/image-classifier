package GroupLabels

import (
	"os"

	"github.com/rwcarlsen/goexif/exif"
)

type DataDateHour struct{}

//Function calculates label statistic value for provided imPath, takes Index provided from Frontend for ordering reasons
//After execution Data contains imIndex at key=calculated statistic, or if it existed previously, imIndex is appended to Duplicates at value = calculated statistic
func (imp *DataDateHour) computeFileStatistic(l *Label, imIndex uint32, imPath string) {
	defer l.waitGroup.Done()

	file, err := os.Open(imPath)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}
	defer file.Close()

	//changed from img to exif
	im, err := exif.Decode(file)
	if err != nil {
		l.mu.Lock()

		_, exists := l.Data[-1]
		if exists {
			l.Duplicates[-1] = append(l.Duplicates[-1], imPath)
		} else {
			l.Data[-1] = imPath
		}

		l.mu.Unlock()
		return
	}

	/////////////////////////////////////////////////////////////////
	// grouping by = (year * 366 * 24) + (mc * 31 * 24 ) + day* 24 + hour
	// maybe year - 2000 ? for optimization
	/////////////////////////////////////////////////////////////////

	var photoDate, _ = im.DateTime()

	var ho = photoDate.Hour()
	var da = photoDate.Day()
	var mc = photoDate.Month()
	var ye = photoDate.Year()

	var dataSum = int32(ho) + int32(da*24) + int32(mc*31*24) + int32(ye*366*24)

	/////////////////////////////////////////////////////////////////

	statistic := dataSum

	//This is a not concurrent safe section, mutex must be locked
	l.mu.Lock()

	_, exists := l.Data[statistic]
	if exists {
		l.Duplicates[statistic] = append(l.Duplicates[statistic], imPath)
	} else {
		l.Data[statistic] = imPath
	}

	l.mu.Unlock()
}

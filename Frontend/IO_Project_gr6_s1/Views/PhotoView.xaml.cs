﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;

namespace IO_Project_gr6_s1.Views
{
    /// <summary>
    /// Interaction logic for PhotoView.xaml
    /// </summary>
    public partial class PhotoView : UserControl
    {
        public PhotoView()
        {
            InitializeComponent();
            string filename = "EditorSaves/" + File.ReadAllText("EditorSaves/connection.txt");
            
            
            if (System.IO.File.Exists(filename))
            {
                System.Diagnostics.Trace.WriteLine("znalazlem plik tekstowy");
                Canvas canvas;
                using (FileStream fs = new FileStream(filename, FileMode.Open))
                {
                    canvas = XamlReader.Load(fs) as Canvas;
                    foreach (UIElement child in canvas.Children)
                    {
                        var copy = CloneXaml(child);
                        EditorCanvas.Children.Add(copy);

                    }

                }
            }
        }
        public static T CloneXaml<T>(T source)
        {
            string xaml = XamlWriter.Save(source);
            StringReader sr = new StringReader(xaml);
            XmlReader xr = XmlReader.Create(sr);
            return (T)XamlReader.Load(xr);

        }
        private void ShowHideAd(object sender, System.Windows.RoutedEventArgs e)
        {
            if(EditorCanvas.Visibility == Visibility.Visible)
                EditorCanvas.Visibility = Visibility.Hidden;
            else
                EditorCanvas.Visibility = Visibility.Visible;
        }
    }
}

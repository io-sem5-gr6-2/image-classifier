﻿using System.Windows.Input;

namespace IO_Project_gr6_s1.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        public HomeViewModel(ICommand _UpdateViewCommand) => this.UpdateViewCommand = _UpdateViewCommand;
        public ICommand UpdateViewCommand { get; set; }
    }
}
